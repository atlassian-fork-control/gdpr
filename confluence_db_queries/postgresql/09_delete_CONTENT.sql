-- Type       : delete
-- Origin     : User Profile Info
-- Description: Deleting User Profile Info
delete from CONTENT where CONTENTTYPE = 'USERINFO' and USERNAME IN (select user_key from user_mapping where username = '__username__' );