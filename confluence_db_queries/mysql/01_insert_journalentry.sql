-- Type       : insert
-- Origin     : Journal Entry
-- Description: Inserting Journal Entry rows to trigger incremental index
INSERT INTO journalentry
                      SELECT @n := @n + 1                                                                AS id
                        ,    'main_index'                                                                AS JOURNAL_NAME
                        ,    CURRENT_TIMESTAMP() - INTERVAL 2 DAY                                        AS CREATIONDATE
                        ,    'DELETE_DOCUMENT'                                                           AS TYPE
                        ,    concat('com.atlassian.confluence.user.PersonalInformation-', tmp.CONTENTID) AS MESSAGE
                      FROM (
                             SELECT CONTENTID
                             FROM CONTENT
                             WHERE CONTENTTYPE = 'USERINFO' AND CONTENT.USERNAME IN (
                               SELECT user_key
                               FROM user_mapping
                               WHERE username =
                                     '__username__')) AS tmp,
                        (
                          SELECT @n := (
                            SELECT max(ENTRY_ID)
                            FROM journalentry)) AS parameter;

INSERT INTO journalentry
                      SELECT @n := @n + 1                                                                AS id
                        ,    'main_index'                                                                AS JOURNAL_NAME
                        ,    CURRENT_TIMESTAMP() - INTERVAL 2 DAY                                        AS CREATIONDATE
                        ,    'DELETE_CHANGE_DOCUMENTS'                                                   AS TYPE
                        ,    concat('com.atlassian.confluence.user.PersonalInformation-', tmp.CONTENTID) AS MESSAGE
                      FROM (
                             SELECT CONTENTID
                             FROM CONTENT
                             WHERE CONTENTTYPE = 'USERINFO' AND CONTENT.USERNAME IN (
                               SELECT user_key
                               FROM user_mapping
                               WHERE username =
                                     '__username__')) AS tmp,
                        (
                          SELECT @n := (
                            SELECT max(ENTRY_ID)
                            FROM journalentry)) AS parameter;

INSERT INTO journalentry (
                      SELECT @n := @n + 1        AS id
                        ,    'main_index'        AS JOURNAL_NAME
                        ,    CURRENT_TIMESTAMP() - INTERVAL 2 DAY AS CREATIONDATE
                        ,    type_name           AS TYPE
                        ,    CASE tmp.CONTENTTYPE
                            WHEN 'PAGE'
                                THEN concat('com.atlassian.confluence.pages.Page-', tmp.CONTENTID)
                            WHEN 'BLOGPOST'
                                THEN concat('com.atlassian.confluence.pages.BlogPost-', tmp.CONTENTID)
                            WHEN 'COMMENT'
                                THEN concat('com.atlassian.confluence.pages.Comment-', tmp.CONTENTID)
                            END                 AS message
                        FROM (
                            SELECT CONTENT.CONTENTID
                                , CONTENT.CONTENTTYPE
                            FROM CONTENT
                                JOIN BODYCONTENT
                                ON CONTENT.CONTENTID = BODYCONTENT.CONTENTID
                            WHERE CONTENT.PREVVER IS NULL
                                    AND CONTENT.CONTENTTYPE IN ('PAGE', 'BLOGPOST', 'COMMENT')
                                    AND CONTENT.CONTENT_STATUS = 'current'
                                    AND BODYCONTENT.BODY LIKE concat('%<ri:user ri:userkey="', (
                                SELECT user_key
                                FROM user_mapping
                                WHERE user_mapping.username = '__username__'), '"%')) AS tmp
                        JOIN (SELECT 'ADD_CHANGE_DOCUMENT' AS type_name
                                UNION SELECT 'UPDATE_DOCUMENT' AS type_name) AS tmp_type
                            ON (1 = 1)
                        ,
                        (
                            SELECT @n := (
                            SELECT max(ENTRY_ID)
                            FROM journalentry)) AS parameter);