-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : postgresql

select "deployment_environment".* from deployment_environment where LOWER(triggers_xml_data) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

