-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : postgresql

select "queue".* from queue where LOWER(title) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

