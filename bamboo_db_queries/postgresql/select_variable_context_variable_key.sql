-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : postgresql

select "variable_context".* from variable_context where LOWER(variable_key) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

