-- Type        : select
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : postgresql

select "imserver".* from imserver where LOWER(resource_name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

