-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "cwd_application_alias".*,lower_alias_name as lower_alias_name_before,REGEXP_REPLACE(lower_alias_name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as lower_alias_name_after from cwd_application_alias where LOWER(lower_alias_name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update cwd_application_alias set lower_alias_name = REGEXP_REPLACE(lower_alias_name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(lower_alias_name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

