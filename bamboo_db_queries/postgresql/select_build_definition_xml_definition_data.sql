-- Type        : select
-- Origin      : bamboo
-- Description : Job configuration
-- Database    : postgresql

select "build_definition".* from build_definition where LOWER(xml_definition_data) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

