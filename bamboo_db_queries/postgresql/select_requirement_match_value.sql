-- Type        : select
-- Origin      : bamboo
-- Description : Job requirements
-- Database    : postgresql

select "requirement".* from requirement where LOWER(match_value) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

