-- Type        : update
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "imserver".*,title as title_before,REGEXP_REPLACE(title,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as title_after from imserver where LOWER(title) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update imserver set title = REGEXP_REPLACE(title,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(title) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

