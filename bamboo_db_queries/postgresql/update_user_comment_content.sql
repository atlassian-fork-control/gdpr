-- Type        : update
-- Origin      : bamboo
-- Description : author of user comments
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "user_comment".*,content as content_before,REGEXP_REPLACE(content,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as content_after from user_comment where LOWER(content) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update user_comment set content = REGEXP_REPLACE(content,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(content) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

