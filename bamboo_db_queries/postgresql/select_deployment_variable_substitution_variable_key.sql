-- Type        : select
-- Origin      : bamboo
-- Description : Deployment variable substitution
-- Database    : postgresql

select "deployment_variable_substitution".* from deployment_variable_substitution where LOWER(variable_key) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

