-- Type        : update
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "deployment_version_item_ba".*,label as label_before,REGEXP_REPLACE(label,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as label_after from deployment_version_item_ba where LOWER(label) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update deployment_version_item_ba set label = REGEXP_REPLACE(label,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(label) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

