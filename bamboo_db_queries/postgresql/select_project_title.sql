-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo project
-- Database    : postgresql

select "project".* from project where LOWER(title) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

