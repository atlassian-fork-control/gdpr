-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd directories
-- Database    : postgresql

select "cwd_directory".* from cwd_directory where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

