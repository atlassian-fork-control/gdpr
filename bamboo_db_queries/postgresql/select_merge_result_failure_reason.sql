-- Type        : select
-- Origin      : bamboo
-- Description : Merge result
-- Database    : postgresql

select "merge_result".* from merge_result where LOWER(failure_reason) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

