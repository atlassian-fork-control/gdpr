-- Type        : select
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : postgresql

select "os_propertyentry".* from os_propertyentry where LOWER(entity_key) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

