-- Type        : update
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "deployment_environment".*,triggers_xml_data as triggers_xml_data_before,REGEXP_REPLACE(triggers_xml_data,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as triggers_xml_data_after from deployment_environment where LOWER(triggers_xml_data) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update deployment_environment set triggers_xml_data = REGEXP_REPLACE(triggers_xml_data,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(triggers_xml_data) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

