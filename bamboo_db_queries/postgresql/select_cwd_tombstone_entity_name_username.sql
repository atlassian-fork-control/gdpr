-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd tombstone
-- Database    : postgresql

select "cwd_tombstone".* from cwd_tombstone where LOWER(entity_name) = LOWER('<OLD_VALUE>') ;

