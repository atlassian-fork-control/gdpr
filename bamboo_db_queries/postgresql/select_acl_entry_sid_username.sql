-- Type        : select
-- Origin      : bamboo
-- Description : user permissions
-- Database    : postgresql

select "acl_entry".* from acl_entry where (LOWER(sid) = LOWER('<OLD_VALUE>') ) AND type  = 'PRINCIPAL';

