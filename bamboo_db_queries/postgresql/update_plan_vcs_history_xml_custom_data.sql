-- Type        : update
-- Origin      : bamboo
-- Description : Merge result
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "plan_vcs_history".*,xml_custom_data as xml_custom_data_before,REGEXP_REPLACE(xml_custom_data,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as xml_custom_data_after from plan_vcs_history where LOWER(xml_custom_data) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update plan_vcs_history set xml_custom_data = REGEXP_REPLACE(xml_custom_data,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(xml_custom_data) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

