-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version item
-- Database    : postgresql

select "deployment_version_item".* from deployment_version_item where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

