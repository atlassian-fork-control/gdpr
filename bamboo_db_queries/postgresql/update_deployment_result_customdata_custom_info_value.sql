-- Type        : update
-- Origin      : bamboo
-- Description : Deployment result additional data
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "deployment_result_customdata".*,custom_info_value as custom_info_value_before,REGEXP_REPLACE(custom_info_value,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as custom_info_value_after from deployment_result_customdata where LOWER(custom_info_value) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update deployment_result_customdata set custom_info_value = REGEXP_REPLACE(custom_info_value,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(custom_info_value) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

