-- Type        : update
-- Origin      : bamboo
-- Description : URL of the trusted app
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "trusted_apps_urls".*,url_pattern as url_pattern_before,'<NEW_VALUE>' as url_pattern_after from trusted_apps_urls where LOWER(url_pattern) like LOWER('%<OLD_VALUE>%') ;

-- + UPDATE (be careful)
update trusted_apps_urls set url_pattern = '<NEW_VALUE>' where LOWER(url_pattern) like LOWER('%<OLD_VALUE>%') ;

