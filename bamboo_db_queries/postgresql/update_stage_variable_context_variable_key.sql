-- Type        : update
-- Origin      : bamboo
-- Description : Value of variables in result stage
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "stage_variable_context".*,variable_key as variable_key_before,REGEXP_REPLACE(variable_key,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as variable_key_after from stage_variable_context where LOWER(variable_key) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update stage_variable_context set variable_key = REGEXP_REPLACE(variable_key,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(variable_key) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

