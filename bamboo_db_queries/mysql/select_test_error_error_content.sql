-- Type        : select
-- Origin      : bamboo
-- Description : Test error content
-- Database    : mysql

select test_error.* from test_error where LOWER(error_content) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

