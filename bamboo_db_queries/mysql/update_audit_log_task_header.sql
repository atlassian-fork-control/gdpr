-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select audit_log.*,task_header as task_header_before,replace(task_header,'<OLD_VALUE>','<NEW_VALUE>') as task_header_after from audit_log where LOWER(task_header) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update audit_log set task_header = replace(task_header,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(task_header) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

