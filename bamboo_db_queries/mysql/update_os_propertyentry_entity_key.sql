-- Type        : update
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select os_propertyentry.*,entity_key as entity_key_before,replace(entity_key,'<OLD_VALUE>','<NEW_VALUE>') as entity_key_after from os_propertyentry where LOWER(entity_key) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update os_propertyentry set entity_key = replace(entity_key,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(entity_key) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

