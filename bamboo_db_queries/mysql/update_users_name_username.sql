-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo internal users directory
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select users.*,name as name_before,'<NEW_VALUE>' as name_after from users where LOWER(name) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update users set name = '<NEW_VALUE>' where LOWER(name) = LOWER('<OLD_VALUE>') ;

