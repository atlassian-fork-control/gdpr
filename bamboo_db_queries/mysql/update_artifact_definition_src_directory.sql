-- Type        : update
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select artifact_definition.*,src_directory as src_directory_before,replace(src_directory,'<OLD_VALUE>','<NEW_VALUE>') as src_directory_after from artifact_definition where LOWER(src_directory) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update artifact_definition set src_directory = replace(src_directory,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(src_directory) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

