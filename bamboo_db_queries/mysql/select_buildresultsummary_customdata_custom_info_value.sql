-- Type        : select
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : mysql

select buildresultsummary_customdata.* from buildresultsummary_customdata where LOWER(custom_info_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

