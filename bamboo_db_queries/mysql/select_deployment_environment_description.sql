-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : mysql

select deployment_environment.* from deployment_environment where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

