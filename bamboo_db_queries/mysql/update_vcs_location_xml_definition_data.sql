-- Type        : update
-- Origin      : bamboo
-- Description : Configuration of vcs repository
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select vcs_location.*,xml_definition_data as xml_definition_data_before,replace(xml_definition_data,'<OLD_VALUE>','<NEW_VALUE>') as xml_definition_data_after from vcs_location where LOWER(xml_definition_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update vcs_location set xml_definition_data = replace(xml_definition_data,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(xml_definition_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

