-- Type        : select
-- Origin      : bamboo
-- Description : reset password tokens requested by user
-- Database    : mysql

select password_reset_token.* from password_reset_token where LOWER(username) = LOWER('<OLD_VALUE>') ;

