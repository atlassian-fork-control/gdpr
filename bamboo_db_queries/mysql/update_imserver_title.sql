-- Type        : update
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select imserver.*,title as title_before,replace(title,'<OLD_VALUE>','<NEW_VALUE>') as title_after from imserver where LOWER(title) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update imserver set title = replace(title,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(title) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

