-- Type        : update
-- Origin      : bamboo
-- Description : Name of the trusted app
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select trusted_apps.*,app_name as app_name_before,replace(app_name,'<OLD_VALUE>','<NEW_VALUE>') as app_name_after from trusted_apps where LOWER(app_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update trusted_apps set app_name = replace(app_name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(app_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

