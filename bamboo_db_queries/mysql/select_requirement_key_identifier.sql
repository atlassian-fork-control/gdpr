-- Type        : select
-- Origin      : bamboo
-- Description : Job requirements
-- Database    : mysql

select requirement.* from requirement where LOWER(key_identifier) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

