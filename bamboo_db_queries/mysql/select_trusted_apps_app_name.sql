-- Type        : select
-- Origin      : bamboo
-- Description : Name of the trusted app
-- Database    : mysql

select trusted_apps.* from trusted_apps where LOWER(app_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

