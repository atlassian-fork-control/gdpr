-- Type        : update
-- Origin      : bamboo
-- Description : Job configuration
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select build_definition.*,xml_definition_data as xml_definition_data_before,replace(xml_definition_data, '<OLD_VALUE>', '<NEW_VALUE>') as xml_definition_data_after from build_definition where xml_definition_data like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update build_definition set xml_definition_data = replace(xml_definition_data, '<OLD_VALUE>', '<NEW_VALUE>') where xml_definition_data like '%<OLD_VALUE>%' ;

