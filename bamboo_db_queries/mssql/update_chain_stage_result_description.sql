-- Type        : update
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select chain_stage_result.*,description as description_before,replace(description, '<OLD_VALUE>', '<NEW_VALUE>') as description_after from chain_stage_result where description like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update chain_stage_result set description = replace(description, '<OLD_VALUE>', '<NEW_VALUE>') where description like '%<OLD_VALUE>%' ;

