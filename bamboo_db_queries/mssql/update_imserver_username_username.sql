-- Type        : update
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select imserver.*,username as username_before,'<NEW_VALUE>' as username_after from imserver where username = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update imserver set username = '<NEW_VALUE>' where username = '<OLD_VALUE>' ;

