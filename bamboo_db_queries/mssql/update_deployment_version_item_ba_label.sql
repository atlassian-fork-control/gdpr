-- Type        : update
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_version_item_ba.*,label as label_before,replace(label, '<OLD_VALUE>', '<NEW_VALUE>') as label_after from deployment_version_item_ba where label like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_version_item_ba set label = replace(label, '<OLD_VALUE>', '<NEW_VALUE>') where label like '%<OLD_VALUE>%' ;

