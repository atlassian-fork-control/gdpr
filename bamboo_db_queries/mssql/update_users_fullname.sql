-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo internal users directory
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select users.*,fullname as fullname_before,replace(fullname, '<OLD_VALUE>', '<NEW_VALUE>') as fullname_after from users where fullname like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update users set fullname = replace(fullname, '<OLD_VALUE>', '<NEW_VALUE>') where fullname like '%<OLD_VALUE>%' ;

