-- Type        : update
-- Origin      : bamboo
-- Description : URL of the trusted app
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select trusted_apps_urls.*,url_pattern as url_pattern_before,'<NEW_VALUE>' as url_pattern_after from trusted_apps_urls where url_pattern like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update trusted_apps_urls set url_pattern = '<NEW_VALUE>' where url_pattern like '%<OLD_VALUE>%' ;

