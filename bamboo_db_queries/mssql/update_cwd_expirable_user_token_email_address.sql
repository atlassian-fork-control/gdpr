-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd user tokens
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_expirable_user_token.*,email_address as email_address_before,'<NEW_VALUE>' as email_address_after from cwd_expirable_user_token where email_address = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_expirable_user_token set email_address = '<NEW_VALUE>' where email_address = '<OLD_VALUE>' ;

