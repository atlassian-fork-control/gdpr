-- Type        : update
-- Origin      : bamboo
-- Description : Test error content
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select test_error.*,error_content as error_content_before,replace(error_content, '<OLD_VALUE>', '<NEW_VALUE>') as error_content_after from test_error where error_content like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update test_error set error_content = replace(error_content, '<OLD_VALUE>', '<NEW_VALUE>') where error_content like '%<OLD_VALUE>%' ;

