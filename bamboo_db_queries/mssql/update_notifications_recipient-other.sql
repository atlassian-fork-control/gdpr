-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo notifications send to user or other recipients
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select notifications.*,recipient as recipient_before,replace(recipient, '<OLD_VALUE>', '<NEW_VALUE>') as recipient_after from notifications where (recipient like '%<OLD_VALUE>%' ) AND recipient_type  <> 'com.atlassian.bamboo.plugin.system.notifications:recipient.user';

-- + UPDATE (be careful)
update notifications set recipient = replace(recipient, '<OLD_VALUE>', '<NEW_VALUE>') where (recipient like '%<OLD_VALUE>%' ) AND recipient_type  <> 'com.atlassian.bamboo.plugin.system.notifications:recipient.user';

