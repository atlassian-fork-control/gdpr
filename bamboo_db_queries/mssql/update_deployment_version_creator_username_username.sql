-- Type        : update
-- Origin      : bamboo
-- Description : deployment version definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_version.*,creator_username as creator_username_before,'<NEW_VALUE>' as creator_username_after from deployment_version where creator_username = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update deployment_version set creator_username = '<NEW_VALUE>' where creator_username = '<OLD_VALUE>' ;

