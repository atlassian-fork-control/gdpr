-- Type        : update
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_environment.*,description as description_before,replace(description, '<OLD_VALUE>', '<NEW_VALUE>') as description_after from deployment_environment where description like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_environment set description = replace(description, '<OLD_VALUE>', '<NEW_VALUE>') where description like '%<OLD_VALUE>%' ;

