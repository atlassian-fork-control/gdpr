-- Type        : update
-- Origin      : bamboo
-- Description : deployment version definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_version.*,plan_branch_name as plan_branch_name_before,replace(plan_branch_name, '<OLD_VALUE>', '<NEW_VALUE>') as plan_branch_name_after from deployment_version where plan_branch_name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_version set plan_branch_name = replace(plan_branch_name, '<OLD_VALUE>', '<NEW_VALUE>') where plan_branch_name like '%<OLD_VALUE>%' ;

