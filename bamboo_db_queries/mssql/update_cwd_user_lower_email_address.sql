-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_user.*,lower_email_address as lower_email_address_before,'<NEW_VALUE>' as lower_email_address_after from cwd_user where lower_email_address = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_user set lower_email_address = '<NEW_VALUE>' where lower_email_address = '<OLD_VALUE>' ;

