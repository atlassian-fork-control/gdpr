-- Type        : update
-- Origin      : bamboo
-- Description : set of various values used by system; may contain username
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select bandana.*,serialized_data as serialized_data_before,replace(serialized_data, '<OLD_VALUE>', '<NEW_VALUE>') as serialized_data_after from bandana where serialized_data like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update bandana set serialized_data = replace(serialized_data, '<OLD_VALUE>', '<NEW_VALUE>') where serialized_data like '%<OLD_VALUE>%' ;

