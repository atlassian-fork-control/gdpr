-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select audit_log.*,old_value as old_value_before,replace(old_value, '<OLD_VALUE>', '<NEW_VALUE>') as old_value_after from audit_log where old_value like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update audit_log set old_value = replace(old_value, '<OLD_VALUE>', '<NEW_VALUE>') where old_value like '%<OLD_VALUE>%' ;

