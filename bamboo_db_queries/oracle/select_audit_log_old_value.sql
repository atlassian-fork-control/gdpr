-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : oracle

select audit_log.* from audit_log where REGEXP_LIKE (LOWER(old_value),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

