-- Type        : select
-- Origin      : bamboo
-- Description : Repository commits comments - imported from vcs repository
-- Database    : oracle

select user_commit.* from user_commit where REGEXP_LIKE (LOWER(commit_comment_clob),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

