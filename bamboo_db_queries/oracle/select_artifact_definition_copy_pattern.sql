-- Type        : select
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : oracle

select artifact_definition.* from artifact_definition where REGEXP_LIKE (LOWER(copy_pattern),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

