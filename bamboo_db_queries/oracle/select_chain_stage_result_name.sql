-- Type        : select
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : oracle

select chain_stage_result.* from chain_stage_result where REGEXP_LIKE (LOWER(name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

