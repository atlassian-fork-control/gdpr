-- Type        : select
-- Origin      : bamboo
-- Description : Elastic image data
-- Database    : oracle

select elastic_image.* from elastic_image where REGEXP_LIKE (LOWER(name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

