-- Type        : update
-- Origin      : bamboo
-- Description : Repository author
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select author.*,author_name as author_name_before,REGEXP_REPLACE(author_name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as author_name_after from author where REGEXP_LIKE (LOWER(author_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update author set author_name = REGEXP_REPLACE(author_name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(author_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

