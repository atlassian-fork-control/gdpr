-- Type        : select
-- Origin      : bamboo
-- Description : Deployment result
-- Database    : oracle

select deployment_result.* from deployment_result where REGEXP_LIKE (LOWER(version_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

