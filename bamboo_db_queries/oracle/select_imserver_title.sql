-- Type        : select
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : oracle

select imserver.* from imserver where REGEXP_LIKE (LOWER(title),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

