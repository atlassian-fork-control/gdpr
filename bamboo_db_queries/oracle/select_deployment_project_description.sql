-- Type        : select
-- Origin      : bamboo
-- Description : Deployment project definition
-- Database    : oracle

select deployment_project.* from deployment_project where REGEXP_LIKE (LOWER(description),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

