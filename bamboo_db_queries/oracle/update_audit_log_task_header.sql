-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select audit_log.*,task_header as task_header_before,REGEXP_REPLACE(task_header,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as task_header_after from audit_log where REGEXP_LIKE (LOWER(task_header),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update audit_log set task_header = REGEXP_REPLACE(task_header,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(task_header),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

