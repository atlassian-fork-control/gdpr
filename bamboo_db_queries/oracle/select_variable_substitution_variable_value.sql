-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : oracle

select variable_substitution.* from variable_substitution where REGEXP_LIKE (LOWER(variable_value),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

