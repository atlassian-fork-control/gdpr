-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo internal users directory
-- Database    : oracle

select users.* from users where REGEXP_LIKE (LOWER(fullname),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

