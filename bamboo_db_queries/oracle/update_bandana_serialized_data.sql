-- Type        : update
-- Origin      : bamboo
-- Description : set of various values used by system; may contain username
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select bandana.*,serialized_data as serialized_data_before,REGEXP_REPLACE(serialized_data,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as serialized_data_after from bandana where REGEXP_LIKE (LOWER(serialized_data),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update bandana set serialized_data = REGEXP_REPLACE(serialized_data,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(serialized_data),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

