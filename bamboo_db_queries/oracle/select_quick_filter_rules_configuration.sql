-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : oracle

select quick_filter_rules.* from quick_filter_rules where REGEXP_LIKE (LOWER(configuration),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

