-- Type        : select
-- Origin      : bamboo
-- Description : Artifact subscription
-- Database    : oracle

select artifact_subscription.* from artifact_subscription where REGEXP_LIKE (LOWER(dst_directory),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

