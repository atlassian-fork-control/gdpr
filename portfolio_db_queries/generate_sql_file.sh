#!/usr/bin/env bash

if [ $# != 1 ];
then
    echo '\nUsage: ./'${0}' [mssql|mysql|oracle|postgresql]\n'
    exit 1
fi

database=${1}

pattern="userkey.sql"
select="select"
update="update"


cat ${database}/${select}*${pattern} > script_${database}_select_${pattern}
cat ${database}/${update}*${pattern} > script_${database}_UPDATE_${pattern}

GLOBIGNORE="*${pattern}"
cat ${database}/${select}* > script_${database}_select.sql
cat ${database}/${update}* > script_${database}_UPDATE.sql
GLOBIGNORE=""

if [ -d "${database}_manual" ];
then
    GLOBIGNORE="*${pattern}"
    cat "${database}_manual"/${select}* >> script_${database}_select.sql
    cat "${database}_manual"/${update}* >> script_${database}_UPDATE.sql
    GLOBIGNORE=""
fi
