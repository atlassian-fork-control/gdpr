d='"'${1}'": {
        "origin": "Jira Portfolio, Live Plan",
        "description": "",
        "action": [
            {
                "type": "select",
                "columns": "*",
                "manual": false
            },
            {
                "type": "update",
                "manual": false
            }
        ],
        "where": {
            "'${2}'": {
                "type": "user_key"
            }
        },
        "active": true
    },'

    echo ${d}
